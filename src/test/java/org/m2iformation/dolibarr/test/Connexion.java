package org.m2iformation.dolibarr.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class Connexion {
	public static ChromeDriver driver;
	@BeforeClass
	public static void setupClass() {
		// On initialise l'objet driver
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Before
	
		public void setup() {
		// lancement � l'adresse
		//Before : je me connecte � l'application
	}
	
	@Test
	
	public void testConnexion() throws InterruptedException {
		//Thread.sleep(5000); permet d'attendre 5secondes apr�s la connexion et avant d'ex�cuter la commande suivante.
		//rajoute "throws InterruptedException"
		// les steps et v�rification du test
		driver.get("http://demo.testlogiciel.pro/dolibarr/");
		driver.findElement(By.id("username")).sendKeys("jsmith");
		driver.findElement(By.name("password")).sendKeys("dolibarrhp");
		driver.findElement(By.xpath("//input[contains(@value,'Connexion')]")).click();
		Thread.sleep(5000);
		//driver.findElement(By.xpath("//input[@value='� Connexion �']")).click();; de chropath
		assertTrue(driver.findElement(By.className("titre")).isDisplayed());
		assertEquals("Espace accueil", driver.findElement(By.className("titre")).getText());
		driver.findElement(By.xpath("//img[@class='login']")).click();
		//div[contains(text(),'Espace accueil')]
	}

	@After
	
	public void tearDown() {
		
}
	@AfterClass

public static void tearDownClass() {
	// On quitte le driver
		// Je me d�connecte � l'application
		//driver.quit();
}
}
